import React, { Component } from 'react';
import {StyleSheet, Image} from 'react-native';
import { Container, Header, Content, Button, Text, Left, Icon, Body, Title, Right, Card, CardItem, Thumbnail, Footer, FooterTab } from 'native-base';
import { Actions } from 'react-native-router-flux';

export default class Home extends Component {
  render() {
    return (
      <Container style={{backgroundColor:"#F8F8F8"}}>
        <Header style={styles.headerStyle}>
          <Left>
            <Button transparent>
              <Icon type="Foundation" name='home' />
            </Button>
          </Left>
          <Body>
            <Title>Home</Title>
          </Body>
          <Right>
            <Button transparent>
              <Icon type="Entypo" name='menu' />
            </Button>
          </Right>
        </Header>
        <Content padder>
          <Card style={styles.cardStyle}>
            <CardItem style={styles.cardItemStyleTop}>
              <Left>
                <Thumbnail source={require('../assets/images/avatar.png')} />
                <Body>
                  <Text>NativeBase</Text>
                  <Text note>GeekyAnts</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image source={{uri: 'https://images.unsplash.com/photo-1600716608141-b935cbcd0c7a?ixlib=rb-1.2.1&auto=format&fit=crop&w=1267&q=80'}} style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            <CardItem style={styles.cardItemStyleBottom}>
              <Left>
                <Button transparent>
                  <Icon active type="FontAwesome" name="thumbs-up" />
                  <Text>12 Likes</Text>
                </Button>
              </Left>
              <Body>
                <Button transparent>
                  <Icon active type="FontAwesome" name="wechat" />
                  <Text>4 Comments</Text>
                </Button>
              </Body>
              <Right>
                <Text>11h ago</Text>
              </Right>
            </CardItem>
          </Card>

          <Card style={styles.cardStyle}>
            <CardItem style={styles.cardItemStyleTop}>
              <Left>
                <Thumbnail source={require('../assets/images/avatar.png')} />
                <Body>
                  <Text>NativeBase</Text>
                  <Text note>GeekyAnts</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image source={{uri: 'https://images.unsplash.com/photo-1569511069263-4aab2754ddf1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1490&q=80'}} style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            <CardItem style={styles.cardItemStyleBottom}>
              <Left>
                <Button transparent>
                  <Icon active type="FontAwesome" name="thumbs-up" />
                  <Text>12 Likes</Text>
                </Button>
              </Left>
              <Body>
                <Button transparent>
                  <Icon active type="FontAwesome" name="wechat" />
                  <Text>4 Comments</Text>
                </Button>
              </Body>
              <Right>
                <Text>11h ago</Text>
              </Right>
            </CardItem>
          </Card>
        </Content>
        <Footer>
          <FooterTab style={styles.footerStyle}>
            <Button>
              <Icon type="AntDesign" name="appstore-o" />
            </Button>
            <Button onPress={()=> Actions.gallery({data:"ini data",data2:"ini data 2"})}>
              <Icon type="AntDesign" name="camera" />
            </Button>
            <Button>
              <Icon type="FontAwesome" name="thumbs-o-up" />
            </Button>
            <Button>
              <Icon type="MaterialIcons" name="person" />
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  headerStyle:{
    backgroundColor:"#24252a",
  },
  footerStyle:{
    backgroundColor:"#24252a",
  },
  cardStyle:{
    borderRadius:10
  },
  cardItemStyleTop:{
    borderTopRightRadius:10, 
    borderTopLeftRadius:10
  },
  cardItemStyleBottom:{
    borderBottomRightRadius:10, 
    borderBottomLeftRadius:10
  }
});