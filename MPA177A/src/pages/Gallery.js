import React, { Component } from 'react';
import {Image} from 'react-native';
import { Container, Header, Content, Button, Text, Left, Icon, Body, Title, Right, Card, CardItem, Thumbnail, Footer, FooterTab } from 'native-base';
import { Actions } from 'react-native-router-flux';
export default class Gallery extends Component {
    constructor(props){
        super(props);
        this.state={
            tangkapData: this.props.data,
        }

    }
  render() {
    //   this.state.tangkapData=this.props.data;
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={()=> Actions.home()}>
              <Icon type="Foundation" name='home' />
            </Button>
          </Left>
          <Body>
            <Title>Gallery-{this.state.tangkapData}</Title>
          </Body>
          <Right>
            <Button transparent>
              <Icon type="Entypo" name='menu' />
            </Button>
          </Right>
        </Header>
        <Content padder>
            <Text>{this.props.data2}</Text>
        <Card>
            <CardItem cardBody>
                <Image source={{uri: 'https://images.unsplash.com/photo-1569511069263-4aab2754ddf1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1490&q=80'}} style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            
        </Card>
        <Card>
            <CardItem cardBody>
                <Image source={{uri: 'https://images.unsplash.com/photo-1569511069263-4aab2754ddf1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1490&q=80'}} style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
        </Card>
        <Card>
            <CardItem cardBody>
                <Image source={{uri: 'https://images.unsplash.com/photo-1569511069263-4aab2754ddf1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1490&q=80'}} style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
        </Card>
        </Content>
        <Footer>
          <FooterTab>
            <Button>
              <Icon type="AntDesign" name="appstore-o" />
            </Button>
            <Button>
              <Icon type="AntDesign" name="camera" />
            </Button>
            <Button>
              <Icon type="FontAwesome" name="thumbs-o-up" />
            </Button>
            <Button>
              <Icon type="MaterialIcons" name="person" />
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}